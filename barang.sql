-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 11, 2019 at 09:02 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `barang`
--

-- --------------------------------------------------------

--
-- Table structure for table `brg`
--

CREATE TABLE `brg` (
  `KodeBarang` varchar(8) NOT NULL,
  `NamaBarang` varchar(70) NOT NULL,
  `StokGudang` int(11) NOT NULL,
  `StokProduksi` int(11) NOT NULL DEFAULT '0',
  `Gambar` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brg`
--

INSERT INTO `brg` (`KodeBarang`, `NamaBarang`, `StokGudang`, `StokProduksi`, `Gambar`) VALUES
('B001', 'Kursi Maintance', 15, 5, 'UseCaseTRANSAKSI.PNG'),
('B003', 'qwerty', 30, 0, 'IMG_5138.JPG');

-- --------------------------------------------------------

--
-- Table structure for table `brg_klr`
--

CREATE TABLE `brg_klr` (
  `no_sj` varchar(8) NOT NULL,
  `id_brg` varchar(8) NOT NULL,
  `nm_brg` varchar(70) NOT NULL,
  `jml_kirim` int(11) NOT NULL,
  `biaya_kirim` int(11) NOT NULL,
  `hrg_jual` int(11) NOT NULL,
  `pengirim` varchar(70) NOT NULL,
  `tujuan` varchar(70) NOT NULL,
  `deskripsi` text NOT NULL,
  `total` int(11) NOT NULL,
  `tgl_kirim` date NOT NULL,
  `status` varchar(20) NOT NULL,
  `kurir` varchar(50) NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brg_klr`
--

INSERT INTO `brg_klr` (`no_sj`, `id_brg`, `nm_brg`, `jml_kirim`, `biaya_kirim`, `hrg_jual`, `pengirim`, `tujuan`, `deskripsi`, `total`, `tgl_kirim`, `status`, `kurir`, `foto`) VALUES
('SJ001', 'B001', 'Kursi Manchester', 4, 20000, 50000, 'PT Kursi Indah', 'PT Jaya Indah', 'sdasdsad', 220000, '2019-05-22', 'Ok', 'Cimon', 'images(2).jpg'),
('SJ002', 'B001', 'Kursi King Classic', 4, 20000, 50000, 'PT Indah Jaya', 'PT Kursi Indah', 'asdasd', 220000, '2019-05-24', 'Ok', 'Muklis', 'ceklis2.jpg'),
('SJ003', 'B001', 'Kursi King Classic', 10, 9000, 70000, 'PT hema', 'rajeg', 'kirim', 709000, '2019-05-28', 'Ok', 'Muklis', '1476099121609.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `brg_msk`
--

CREATE TABLE `brg_msk` (
  `id` int(11) NOT NULL,
  `id_brg` varchar(8) NOT NULL,
  `nm_brg` varchar(70) NOT NULL DEFAULT '',
  `tgl_msk` date NOT NULL,
  `jml_beli` int(11) NOT NULL,
  `hrg_beli` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `pengirim` varchar(70) NOT NULL DEFAULT '',
  `penerima` varchar(70) NOT NULL,
  `biaya_kirim` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brg_msk`
--

INSERT INTO `brg_msk` (`id`, `id_brg`, `nm_brg`, `tgl_msk`, `jml_beli`, `hrg_beli`, `total`, `foto`, `deskripsi`, `pengirim`, `penerima`, `biaya_kirim`) VALUES
(27, 'B001', 'Kursi King Classic', '2019-05-24', 12, 40000, 500000, 'Desert.jpg', 'SASASDSA', 'PT Indah Jaya', 'Pt Indah', 20000),
(28, 'B001', 'Kursi King Classic', '2019-05-22', 12, 40000, 500000, '', 'SASASDSA', 'PT Indah Jaya', 'Pt Indah', 20000),
(29, 'B002', 'kursi goyang', '2019-05-28', 7, 50000, 0, '1475724977507.jpg', 'barang bagus', 'PT hema', '', 0),
(30, 'B001', 'Kursi King Classic', '2019-05-28', 12, 40000, 0, '', 'SASASDSA', 'PT Indah Jaya', 'pt hema', 0),
(31, 'B001', 'Kursi King Classic', '2019-05-29', 12, 40000, 0, '', 'SASASDSA', 'PT Indah Jaya', 'hema', 0),
(32, 'B001', 'Kursi King Classic', '2019-05-22', 12, 40000, 489000, '', 'SASASDSA', 'PT Indah Jaya', 'pt hema', 9000);

-- --------------------------------------------------------

--
-- Table structure for table `g_kirim`
--

CREATE TABLE `g_kirim` (
  `SJGudang` varchar(20) NOT NULL DEFAULT '',
  `AsalTugas` varchar(10) DEFAULT NULL,
  `NamaPetugas` varchar(50) DEFAULT NULL,
  `Tgl_SuratTugas` date DEFAULT NULL,
  `NamaPemesan` varchar(50) DEFAULT NULL,
  `AlamatPemesan` varchar(255) DEFAULT NULL,
  `TglKirim` date DEFAULT NULL,
  `TglTerima` date DEFAULT NULL,
  `KodeBarang` varchar(10) DEFAULT NULL,
  `JumlahBeli` int(11) DEFAULT NULL,
  `HargaBarang` int(11) DEFAULT NULL,
  `Total` int(11) DEFAULT NULL,
  `KodeKurir` varchar(10) DEFAULT NULL,
  `KodePetugas` varchar(10) DEFAULT NULL,
  `GambarSurat` varchar(255) DEFAULT NULL,
  `Deskripsi` varchar(255) DEFAULT NULL,
  `Status` varchar(20) DEFAULT NULL,
  `GambarPenerima` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `g_kirim`
--

INSERT INTO `g_kirim` (`SJGudang`, `AsalTugas`, `NamaPetugas`, `Tgl_SuratTugas`, `NamaPemesan`, `AlamatPemesan`, `TglKirim`, `TglTerima`, `KodeBarang`, `JumlahBeli`, `HargaBarang`, `Total`, `KodeKurir`, `KodePetugas`, `GambarSurat`, `Deskripsi`, `Status`, `GambarPenerima`) VALUES
('ASAL/001', 'Marketing', 'Jojo', '2019-06-04', 'Ilham', 'Parkotek', '2019-06-06', '2019-06-07', 'B003', 1, 2000, 2000, 'K005', 'K006', 'IMG_5138.JPG', 'Cepet Ya Gan..', 'Terima', 'IMG_5136.JPG');

-- --------------------------------------------------------

--
-- Table structure for table `histori`
--

CREATE TABLE `histori` (
  `id` int(11) NOT NULL,
  `id_brg` varchar(8) NOT NULL,
  `nm_brg` varchar(70) NOT NULL,
  `jml_beli` int(11) NOT NULL,
  `hrg_beli` int(11) NOT NULL,
  `biaya_kirim` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `desc_pesan` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `pengirim` varchar(70) NOT NULL DEFAULT '',
  `foto` varchar(100) NOT NULL,
  `baru` varchar(30) NOT NULL,
  `stok_lama` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `histori`
--

INSERT INTO `histori` (`id`, `id_brg`, `nm_brg`, `jml_beli`, `hrg_beli`, `biaya_kirim`, `total`, `desc_pesan`, `status`, `pengirim`, `foto`, `baru`, `stok_lama`) VALUES
(23, 'B001', 'Kursi King Classic', 12, 40000, 0, 480000, 'SASASDSA', 'Ok', 'PT Indah Jaya', 'Desert.jpg', 'Ok', 0),
(24, 'B001', 'Kursi King Classic', 12, 40000, 20000, 500000, 'kURSI Biru', 'Ok', 'PT Indah Jaya', '', '', 8),
(25, 'B002', 'kursi goyang', 7, 50000, 0, 0, 'barang bagus', 'Konfirmasi', 'PT hema', '1475724977507.jpg', 'Ok', 0),
(26, 'B002', 'kursi goyang', 9, 50000, 9000, 459000, 'ga jelas\r\n', 'Konfirmasi', 'PT Hema', '', '', 7);

-- --------------------------------------------------------

--
-- Table structure for table `k_barang`
--

CREATE TABLE `k_barang` (
  `KodeKonfirmasi` varchar(10) NOT NULL,
  `KodeKirim` varchar(10) NOT NULL,
  `TglPeriksa` date NOT NULL,
  `JumlahDiterima` int(11) NOT NULL,
  `JumlahDitolak` int(11) NOT NULL,
  `TotalBarang` int(11) NOT NULL,
  `KodeProduksi` varchar(10) NOT NULL,
  `KodeGudang` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(8) NOT NULL,
  `level` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `username`, `password`, `level`) VALUES
(1, 'admin', 'admin', 'admin'),
(4, 'gudang', 'gudang', 'gudang'),
(6, 'kurir', 'kurir', 'kurir'),
(7, 'produksi', 'produksi', 'produksi');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` varchar(8) NOT NULL,
  `nm_pegawai` varchar(70) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jenkel` varchar(12) NOT NULL,
  `nohp` varchar(13) NOT NULL,
  `bagian` varchar(15) NOT NULL,
  `alamat` text NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nm_pegawai`, `tgl_lahir`, `jenkel`, `nohp`, `bagian`, `alamat`, `username`, `password`) VALUES
('K001', 'Ogel Bae', '2019-05-17', 'Laki-Laki', '08956115544', 'Gudang', 'Tangerang', 'gudang', 'gudang'),
('K002', 'Cimon', '2019-05-19', 'Laki-Laki', '08956111', 'Kurir', 'Tangerang', 'kurir', 'kurir'),
('K003', 'Muklis', '2019-05-24', 'Laki-Laki', '0873427', 'Admin', 'asdasdasd', 'admin', 'admin'),
('K004', 'K_Produksi', '1996-10-01', 'Laki-Laki', '0987654321', 'Produksi', 'Tangerang', 'produksi', 'produksi'),
('K005', 'Asal-Kurir', '2019-06-04', 'Laki-Laki', '098976758453', 'Kurir', 'Tangerang\r\n', 'Asal', 'Asal'),
('K006', 'Asal-Gudang', '1996-07-12', 'Perempuan', '081234567891', 'Gudang', 'Tangerang', 'AsalG', 'AsalG'),
('K007', 'Asal-P', '1994-04-03', 'Laki-Laki', '086754367635', 'Produksi', 'Tangerang', 'AsalP', 'AsalP');

-- --------------------------------------------------------

--
-- Table structure for table `p_barang`
--

CREATE TABLE `p_barang` (
  `KodeKirim` varchar(10) NOT NULL,
  `Gambar` varchar(255) DEFAULT NULL,
  `KodeBarang` varchar(10) DEFAULT NULL,
  `NamaBarang` varchar(50) NOT NULL,
  `JumlahDiterima` int(11) DEFAULT NULL,
  `JumlahDitolak` int(11) DEFAULT NULL,
  `JumlahBarang` int(11) NOT NULL,
  `TanggalKirim` date NOT NULL,
  `TanggalKonfirmasi` date DEFAULT NULL,
  `UserProduksi` varchar(10) NOT NULL,
  `UserGudang` varchar(255) DEFAULT NULL,
  `Status` varchar(20) DEFAULT NULL,
  `Keterangan` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `p_barang`
--

INSERT INTO `p_barang` (`KodeKirim`, `Gambar`, `KodeBarang`, `NamaBarang`, `JumlahDiterima`, `JumlahDitolak`, `JumlahBarang`, `TanggalKirim`, `TanggalKonfirmasi`, `UserProduksi`, `UserGudang`, `Status`, `Keterangan`) VALUES
('KK0001', 'IMG_5137.JPG', 'B003', 'qwerty', 10, 5, 20, '2019-06-12', '2019-06-07', 'K004', 'K001', 'Konfirmasi', 'Baru'),
('KK0002', 'UseCaseTRANSAKSI.PNG', 'B003', 'qwerty', 20, 0, 20, '2019-06-12', '2019-06-12', 'K004', 'gudang', 'Konfirmasi', 'Lama'),
('KK0003', 'IMG_5136.JPG', 'B001', 'Kursi Maintance', 15, 5, 20, '2019-06-12', '2019-06-13', 'K004', 'K006', 'Konfirmasi', 'Baru');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brg`
--
ALTER TABLE `brg`
  ADD PRIMARY KEY (`KodeBarang`);

--
-- Indexes for table `brg_klr`
--
ALTER TABLE `brg_klr`
  ADD PRIMARY KEY (`no_sj`);

--
-- Indexes for table `brg_msk`
--
ALTER TABLE `brg_msk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `g_kirim`
--
ALTER TABLE `g_kirim`
  ADD PRIMARY KEY (`SJGudang`);

--
-- Indexes for table `histori`
--
ALTER TABLE `histori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `k_barang`
--
ALTER TABLE `k_barang`
  ADD PRIMARY KEY (`KodeKonfirmasi`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `p_barang`
--
ALTER TABLE `p_barang`
  ADD PRIMARY KEY (`KodeKirim`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brg_msk`
--
ALTER TABLE `brg_msk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `histori`
--
ALTER TABLE `histori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
