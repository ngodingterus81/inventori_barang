-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 04, 2018 at 06:00 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `komplain`
--

-- --------------------------------------------------------

--
-- Table structure for table `dataclient`
--

CREATE TABLE `dataclient` (
  `id` int(11) NOT NULL,
  `namadepan` varchar(20) NOT NULL,
  `namabelakang` varchar(20) NOT NULL,
  `tgl-lahir` date NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `notelp` varchar(15) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dataclient`
--

INSERT INTO `dataclient` (`id`, `namadepan`, `namabelakang`, `tgl-lahir`, `alamat`, `notelp`, `level`) VALUES
(1, 'Yosep', 'Aldi', '1999-09-12', 'Poris Jaya - Tangerang', '081289124733', 2),
(2, 'Aldi', 'Herdiansyah', '1996-03-09', 'Batu Ceper', '08914327654', 1);

-- --------------------------------------------------------

--
-- Table structure for table `komclient`
--

CREATE TABLE `komclient` (
  `idkomp` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `judul_komplain` varchar(50) NOT NULL,
  `isi` text NOT NULL,
  `tgl` date NOT NULL,
  `status` varchar(20) NOT NULL,
  `tgl_selesai` date NOT NULL,
  `analisa` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `komclient`
--

INSERT INTO `komclient` (`idkomp`, `id`, `judul_komplain`, `isi`, `tgl`, `status`, `tgl_selesai`, `analisa`) VALUES
(17, 10, 'Sakit Hatiiieee', 'Di Duain Sama Mantan....', '2018-11-04', 'menunggu', '0000-00-00', ''),
(18, 10, 'Putus Cinta', 'Ketauan Selingkuh', '2018-11-04', 'menunggu', '0000-00-00', '');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(10) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `username`, `password`, `level`) VALUES
(1, 'admin', 'admin', 1),
(10, 'user', 'user', 2),
(11, 'admin123', 'admin123', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dataclient`
--
ALTER TABLE `dataclient`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `komclient`
--
ALTER TABLE `komclient`
  ADD PRIMARY KEY (`idkomp`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dataclient`
--
ALTER TABLE `dataclient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `komclient`
--
ALTER TABLE `komclient`
  MODIFY `idkomp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
