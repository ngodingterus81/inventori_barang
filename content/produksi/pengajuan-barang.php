<?php
    if(isset($_SESSION['p_simpan'])){
        if($_SESSION['p_simpan'] == 'Berhasil'){
            echo'<div class="alert alert-success">
                Berhasil Membuat Permohonan Untuk Menyimpan Barang Ke Gudang..!!
            </div>';
            unset($_SESSION['p_simpan']);
        }
        elseif($_SESSION['p_simpan'] == 'Gagal Foto'){
            echo'<div class="alert alert-warning">
                Berhasil Menyimpan Data Barang Tapi Gagal Menyimpan Foto..!
            </div>';
            unset($_SESSION['p_simpan']);
        }
        else{
            echo'<div class="alert alert-danger">
                Gagal Membuat Permohonan Untuk Menyimpan Barang Ke Gudang..!!
            </div>';
            unset($_SESSION['p_simpan']);
        }
    }

    elseif(isset($_SESSION['g_simpan'])){
        if($_SESSION['g_simpan'] == 'Berhasil'){
            echo'<div class="alert alert-success">
                Berhasil Menyetujui Permintaan..!!
            </div>';
            unset($_SESSION['g_simpan']);
        }
    }

    elseif(isset($_SESSION['u_simpan'])){
        if($_SESSION['u_simpan']=='Berhasil'){
            echo'<div class="alert alert-success">
                Berhasil Mengubah Permintaan..!!
            </div>';
            unset($_SESSION['u_simpan']);
        }
        elseif($_SESSION['u_simpan'] == 'Gagal Foto'){
            echo'<div class="alert alert-warning">
                Berhasil Merubah Data Barang Tapi Gagal Menyimpan Foto..!
            </div>';
            unset($_SESSION['u_simpan']);
        }
        else{
            echo'<div class="alert alert-danger">
                Gagal Merubah Permintaan..!!
            </div>';
            unset($_SESSION['u_simpan']);
        }
    }

    elseif(isset($_SESSION['h_simpan'])){
         if($_SESSION['h_simpan']=='Berhasil'){
            echo'<div class="alert alert-success">
                Berhasil Menghapus Permintaan..!!
            </div>';
            unset($_SESSION['h_simpan']);
        }
        else{
            echo'<div class="alert alert-danger">
                Gagal Menghapus Permintaan..!!
            </div>';
            unset($_SESSION['h_simpan']);
        }
    }
    if($_SESSION['level']=='Produksi'){
?>
<div class="pl-3 mt-2">
    <h3>Pengajuan Barang</h3>
    <span><b>Bagian Produksi</b></span>
</div>
<?php } elseif ($_SESSION['level']=='Gudang') { ?>
<div class="pl-3 mt-2">
    <h3>Daftar Pengajuan Barang</h3>
    <span><b>Bagian Gudang</b></span>
</div>
<?php } ?>
<div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Kode Kirim <br> Kode</th>
                                            <th>Nama Barang</th>
                                            <th>Diterima <br> Ditolak</th>
                                            <th>Jumlah</th>
                                            <th>Pengajuan <br> Konfirmasi</th>
                                            <?php if($_SESSION['level']=='Produksi'){ ?>
                                                <th>Gudang</th>
                                            <?php }else{ ?>
                                                <th>Produksi</th>
                                            <?php } ?>
                                            <th>Status</th>
                                            <?php if($_SESSION['level']=='Produksi'){ ?>
                                                <th>Aksi</th>
                                            <?php } ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $query = mysql_query("SELECT a.KodeKirim, a.KodeBarang, a.NamaBarang, a.JumlahDiterima, a.JumlahDitolak, a.JumlahBarang, a.TanggalKirim, a.TanggalKonfirmasi, a.UserProduksi, b.nm_pegawai AS NmProduksi, a.UserGudang, a.Status FROM p_barang a JOIN pegawai b ON a.UserProduksi=b.id_pegawai ORDER BY a.Status = 'Menunggu' DESC, a.TanggalKirim DESC");$a = 1; while($data=mysql_fetch_array($query)){ ?>
                                            <tr>
                                                <td><?=$a++;?></td>
                                                <td><?=$data['KodeKirim']?><hr><?php if($data['KodeBarang']==''){echo "-";}else{echo $data['KodeBarang'];}?></td>
                                                <td><?=$data['NamaBarang'];?></td>
                                                <td><?php if($data['JumlahDiterima']==''){echo "-";}else{echo $data['JumlahDiterima'];}?><hr><?php if($data['JumlahDitolak']==''){echo "-";}else{echo $data['JumlahDitolak'];}?></td>
                                                <td><?=$data['JumlahBarang']?></td>
                                                <td><?=$data['TanggalKirim']?><hr><?php if($data['TanggalKonfirmasi']==''){echo "-";}else{echo $data['TanggalKonfirmasi'];}?></td>
                                                <?php if($_SESSION['level']=='Produksi'){ ?>
                                                    <td><?php if($data['UserGudang']==""){echo "-";}else{echo $data['UserGudang'];} ?></td>
                                                <?php }else{ ?>
                                                    <td><?php if($data['UserProduksi']==""){echo "-";}else{echo $data['NmProduksi'];} ?></td>
                                                <?php } ?>
                                                <?php if($_SESSION['level']=='Produksi'){?>
                                                    <td>
                                                        <?php if($data['Status']=="Menunggu"){echo'<button class="btn btn-warning btn-sm disabled">Menunggu</button>';}elseif($data['Status']=="Konfirmasi"){echo "Sukses";} ?>
                                                    </td>
                                                <?php }elseif($_SESSION['level']=='Gudang'){ ?>
                                                    <td>
                                                        <?php if($data['Status']=="Menunggu"){echo'<a href="?view=K_produksi_barang&Kode='.$data['KodeKirim'].'"><button class="btn btn-success btn-sm disabled">Konfirmasi</button></a>';}elseif($data['Status']=="Konfirmasi"){echo "Sukses";}?>
                                                    </td>

                                                <?php } if($_SESSION['level']=='Produksi'){?>
                                                    <td align="center">
                                                        <?php if($data['Status']=='Menunggu'){ ?>
                                                            <?php if($_SESSION['id']==$data['UserProduksi']){ ?>
                                                            <a href="?view=ubah_masuk_barang&produksi=ubah&Kode=<?=$data['KodeKirim']?>"><button class="btn btn-primary btn-sm">Ubah</button></a><p></p>
                                                            <button class="btn btn-danger btn-sm" onclick="hapus('<?=$data['KodeKirim']?>')">Hapus</button>
                                                            <?php }else{
                                                                echo '<button class="btn btn-dark btn-sm disabled">No Aksi</button>';
                                                            } ?>
                                                        <?php }else{ ?>
                                                            <button class="btn btn-dark btn-sm disabled">No Aksi</button>
                                                        <?php } ?>
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                </div>
            </div><!-- .animated -->
        </div><!-- .content -->

        <script>
            function hapus(Kode){
                let kon = confirm("Yakin Akan Menarik Permintaan..!!");
                if(kon == true){
                    location.href='?view=proses-produksi&produksi=Hapus&Kode='+Kode;
                }
            }
        </script>