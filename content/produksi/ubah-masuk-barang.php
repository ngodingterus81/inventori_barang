<?php 

$query = mysql_query("SELECT * FROM p_barang WHERE KodeKirim = '$_GET[Kode]'");
$data = mysql_fetch_array($query);

if ($_GET['produksi']=='ubah') {
	if($_SESSION['id']==$data['UserProduksi']){ ?>
		<div class="animated fadeIn"><br>
		    <div class="card">
		        <div class="card-header">
		            <center><h3><strong>Form Pengajuan Barang Baru</strong><h3></center>
		        </div>         
		        <div class="card-body">
		            <form class="col-md-12" method="POST" action="?view=proses-produksi&produksi=u_barang" enctype="multipart/form-data">
		                <div class="row">
		                    <div class="col-md-12">
		                        <div class="col-md-3 pt-2">
		                            <img id="keluaran" src="images/<?=$data['Gambar']?>" class="col-md-12" alt="Foto" width="200" height="195">
		                            <br><p></p>
		                            <input type="file" name="gambar_barang_baru" class="form-control" id="gambar_barang_baru" accept="img/*" onchange="loadfile(event)" hidden="">
		                            <button class="col-12 btn btn-outline-primary" type="button" id="btn_ubah_gambar" onclick="klik('gambar_barang_baru')"><i class="fa fa-plus"></i> Gambar</button>
		                        </div>
		                        <div class="row">
		                            <div class="col-md-6">
		                                <label for="KodeKirim" class="col-md-12"><b>Kode Kirim</b></label>
		                                <input type="text" class="form-control" name="KodeKirim" id="KodeKirim" value="<?=$_GET['Kode'];?>" readonly>
		                            </div>
		                            <div class="col-md-6">
		                                <label for="JumlahBarang" class="col-md-12"><b>Jumlah Barang</b></label>
		                                <input type="number" class="form-control" name="JumlahBarang" id="JumlahBarang" value="<?=$data['JumlahBarang']?>" required>
		                            </div>
		                        </div><br>
		                        <div class="row">
		                            <div class="col-md-12">
		                                <label for="NamaBarang" class="col-md-12"><b>Nama Barang</b></label>
		                                <input type="text" class="form-control" name="NamaBarang" id="NamaBarang" value="<?=$data['NamaBarang']?>" required>
		                            </div>
		                        </div><br>
		                        <div class="row">
		                            <div class="col-md-6">
		                                <label for="TanggalKirim" class="col-md-12"><b>Tanggal Permohonan</b></label>
		                                <input type="date" class="col-md-12 form-control" name="TanggalKirim" required id="TanggalKirim" value="<?=$data['TanggalKirim']?>">
		                            </div>
		                            <div class="col-md-6">
		                                <label for="Tertanda" class="col-md-12 text-center"><b>Tertanda</b></label>
		                                <input type="text" class="col-md-12 form-control text-center" name="Tertanda" required id="Tertanda" value="<?=$data['UserProduksi']?>" readonly>
		                            </div>
		                        </div>
		                    </div>
		                </div><br><hr>
		                <div class="row">
		                    <button class="col-md-6 btn btn-success" type="submit">BUAT PERUBAHAN</button>
		                    <label for="" class="col-md-1">&nbsp;</label>
		                    <a href="index.php?view=p_pengajuan_barang" class="col-md-5">
		                        <button class="col-md-12 btn btn-dark" type="button">KEMBALI / BATAL</button>
		                    </a>
		                </div>
		            </form>
		        </div>
		    </div>
		</div>
		<?php
	}
	else{ ?>
		<br><br><center><h2>Maaf!! Anda Hanya Bisa Mengubah Data Yang Bersangkutan..!!</h2><br><button class="btn btn-primary" id="kembali"><i class="fa fa-reply"></i> Kembali</button></center>
		<?php
	}
}

?>

<script>
	const kembali = document.getElementById("kembali");
	kembali.addEventListener("click", function(){
		window.history.back();
	});

	function klik(ide){
		
		const file = document.getElementById(ide);
		file.click();
	}

    let loadfile = function(event) {
            var output = document.getElementById('keluaran');
            output.src = URL.createObjectURL(event.target.files[0]);
        }
</script>