<div class="animated fadeIn">
	<div class="card">
<div class="card-header">
                                <center><h3><strong>Barang Sudah Diantar</strong><h3></center>
                            </div>
&nbsp;&nbsp;&nbsp;<a href="?view=kirim"><button class="btn btn-primary"><i class="ti-pencil-alt"></i> Kirim Barang</button></a><br/>
<table class="table table-striped table-bordered">
<tr>
	<th>No</th>
	<th>Surat Jalan</th>
	<th>Nama Barang</th>
	<th>Jumlah Dikirim</th>
	<th>Tanggal Kirim</th>
	<th>Tujuan</th>
	<th>Total</th>
	<th>Opsi</th>
</tr>


<?php
include"koneksi.php";
$no=1;
$c=mysql_query("select * from brg_klr");
while($u=mysql_fetch_array($c)){
?>
<tr>
	<td><?php echo $no++ ?></td>
	<td><?php echo $u['no_sj'] ?></td>
	<td><?php echo $u['nm_brg'] ?></td>
	<td><?php echo $u['jml_kirim'] ?></td>
	<td><?php echo date('d F Y', strtotime($u['tgl_kirim'])) ?></td>
	<td><?php echo $u['tujuan'] ?></td>
	<td>Rp. <?php echo number_format($u['total']) ?></td>
	<td><?php if($u['status']=='Dikirim') {?>
		<button disabled="" class="btn btn-danger"><i class="ti-alert"></i>&nbsp;&nbsp;Dikirim</button>
		<a href="?view=edit_brg_klr&no_sj=<?php echo $u['no_sj'] ?>" class="btn btn-warning"><i class="ti-pencil-alt"></i>&nbsp;&nbsp;Edit</a>
		<a href="?view=hapus_brg_klr&no_sj=<?php echo $u['no_sj'] ?>" class="btn btn-danger"><i class="ti-trash"></i>&nbsp;&nbsp;Tolak</a>
	<?php }elseif($u['status']=='Diterima') { ?>
		<button disabled="" class="btn btn-success"><i class="ti-check"></i>&nbsp;&nbsp;Diterima</button>
	<?php } ?>
	</td>
</tr>
<?php } ?>
</table>
</div>
</div>