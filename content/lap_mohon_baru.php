             <div class="animated fadeIn">
                        <div class="card">
                            <div class="card-header">
                                <strong>Mohon Barang Baru</strong>
                            </div>
                            <form method="post" enctype="multipart/form-data">
                            <div class="card-body card-block">
                                <div class="form-group">
                                    <label class=" form-control-label">Nama Barang</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-agenda"></i></div>
                                        <input class="form-control" type="text" name="nm_brg" placeholder="Nama Barang ..">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class=" form-control-label">Harga Barang</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                        <input class="form-control" id="harga" type="number" name="hrg_beli" placeholder="Harga Beli ..">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class=" form-control-label">Pengirim</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-truck"></i></div>
                                        <input class="form-control" type="text" name="pengirim" placeholder="Pengirim ..">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class=" form-control-label">Foto</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-image"></i></div>
                                        <input class="form-control" type="file" name="foto" placeholder="Password ..">
                                    </div>
                                </div>
                                <small class="form-text text-muted">Ukuran Foto Tidak Boleh Lebih Dari 1MB</small><br/>
                                <div class="form-group">
                                    <label class=" form-control-label">Deskripsi</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-clip"></i></div>
                                        <textarea name="desc_pesan" class="form-control"></textarea>
                                    </div>
                                </div>
                                <button type="submit" name="simpan" class="btn btn-primary"><i class="ti-save"></i>&nbsp;&nbsp;&nbsp;Simpan</button>
                                <a href="?view=brg_msk" class="btn btn-danger"><i class="ti-arrow-left"></i>&nbsp;&nbsp;Batal</a>
                            </div>
                        </form>
                    </div>
                </div>

                <?php
                    if(isset($_POST['simpan'])){
                        $id = $_POST['id'];
                        $nm_brg = $_POST['nm_brg'];
                        $pengirim = $_POST['pengirim'];
                        $hrg_beli = $_POST['hrg_beli'];
                        $desc_pesan = $_POST['desc_pesan'];
                        $baru = $_POST['baru'];
                        $ekstensi_diperbolehkan = array('jpg','png');
                        $foto = $_FILES['foto']['name'];
                        $x = explode('.', $foto);
                        $ekstensi = strtolower(end($x));
                        $ukuran = $_FILES['foto']['size'];
                        $file_tmp = $_FILES['foto']['tmp_name'];
                        if(in_array($ekstensi, $ekstensi_diperbolehkan) === true){
                            if($ukuran < 1044070){
                                move_uploaded_file($file_tmp, 'images/'.$foto);
                                $query = mysql_query("insert into histori(id,nm_brg,hrg_beli,desc_pesan,pengirim,foto,baru) values('','$nm_brg','$hrg_beli','$desc_pesan','$pengirim','$foto','Baru')");
                                if($query){
                                    echo"<script>alert ('Data Berhasil Disimpan')</script>";
                                    echo"<meta http-equiv='refresh' content=0;URL=?view=tampil_brg_baru>";
                                }else{
                                    echo"<script>alert ('Data Berhasil Gagal Disimpan')</script>";
                                    echo"<meta http-equiv='refresh' content=0;URL=?view=lap_brg_baru>";
                                }
                            }
                        }
                    }
                    ?>