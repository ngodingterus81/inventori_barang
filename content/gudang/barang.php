<?php if($_GET['tampil']=='' OR $_GET['tampil']=='barang'){ ?>
	<?php 
		if(isset($_SESSION['h_barang'])){
			if($_SESSION['h_barang'] == 'Berhasil'){
				echo '<div class="alert alert-success">Berhasil Menghapus 1 Data Barang..!!</div>';
				unset($_SESSION['h_barang']);
			}
			else{
				echo '<div class="alert alert-danger">Gagal Menghapus Data Barang..!!</div>';
				unset($_SESSION['h_barang']);
			}
		}
		elseif(isset($_SESSION['u_barang'])){
			echo '<div class="alert alert-success">Berhasil Mengubah 1 Data Barang..!!</div>';
			unset($_SESSION['u_barang']);
		}
	?>

<div class="animated fadeIn">
	<div class="card">
		<div class="card-header">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-5" style="width:100%;float:left;">
						<h3><strong>Data Barang</strong><h3>
					</div>
					<div class="col-md-5" style="width:100%;float:right;">
						<input type="text" class="form-control col-md-12" id="cari" placeholder="Pencarian">	
					</div>
					<div class="col-md-1" style="width:100%;float:right;">
						<button class="col-md-12 btn btn-warning" id="btn_cari">Cari</button>
					</div>
					<div class="col-md-1" style="width:100%;float:right;">
						<a href="index.php?view=barang" class="col-md-12 btn btn-primary"> Reset</a>
					</div>
				</div>
			</div>
		</div><br>
		<?php if($_SESSION['level']=='Produksi'){ ?>
			<a href="?view=p_masuk-barang-lama">
                <button class="btn btn-success"><i class="fa fa-plus-circle"></i> Data Barang</button>
            </a><br>
        <?php } ?>
			<table class="table table-striped table-bordered text-center">
			<tr>
				<th>No</th>
				<!-- <th>Harga</th> -->
				<!-- AKSES ADMIN -->
				<?php if($_SESSION['level']=='Admin') { ?>
					<th>Gambar</th>
					<th>Nama Barang</th>
					<th>Stok Produksi</th>
					<th>Stok Gudang</th>
				<?php } ?>
				<!-- AKSES GUDANG -->
				<?php if($_SESSION['level']=='Gudang') { ?>
					<th>Gambar</th>
					<th>Nama Barang</th>
					<th>Stok Gudang</th>
					<th>Opsi</th>
				<?php } ?>
				<!-- AKSES PRODUKSI -->
				<?php if($_SESSION['level']=='Produksi') { ?>
					<th>Gambar</th>
					<th>Nama Barang</th>
					<th>Stok Gudang</th>
					<th>Stok Produksi</th>
					<th>Masuk</th>
				<?php } ?>
			</tr>
				<?php
					$no=1;
					if(isset($_GET['cari'])){
						$c=mysqli_query($con,"select * from brg WHERE KodeBarang LIKE '%$_GET[cari]%' OR NamaBarang LIKE '%$_GET[cari]%'");
					}
					else{
						$c=mysqli_query($con,"select * from brg");
					}
					$cek = mysqli_num_rows($c);
					if($cek <= 0){
						//PRODUKSI
						echo'<tr><td colspan="4"><center><h3><b>Data Kosong</b></h3></center></td></tr>';
					}
					else{
					while($u=mysqli_fetch_array($c)){
						if($u['Gambar']==''){
							$foto = 'nogambar.png';
						}
						else{
							$foto = $u['Gambar'];
						}
				?>
			<tr>
				<td><?php echo $no++ ?></td>
				<td><img src="images/<?=$foto;?>" alt="foto" width="50" height="50"> ( <?=$u['KodeBarang']?> )</td>
				<td><?php echo $u['NamaBarang'] ?></td>
				<td><?php echo $u['StokGudang'] ?></td>
				<?php if($_SESSION['level']=='Produksi' || $_SESSION['level']=='Admin'){ ?>
					<td><?=$u['StokProduksi']?></td>
				<?php } if($_SESSION['level'] != 'Admin'){ ?>
				<td>
					<?php if($_SESSION['level']=='Produksi'){ ?>
						<a href="?view=p_masuk-barang-lama&Kode=<?=$u['KodeBarang']?>"><button class="btn btn-success"><i class="fa fa-plus"></i> Gudang</button></a>
					<?php }elseif($_SESSION['level']=='Gudang'){ ?>
						<a href="?view=barang&tampil=detail&KodeBarang=<?php echo $u['KodeBarang'] ?>" class="btn btn-primary"><i class="ti-eye">&nbsp;&nbsp;View</i></a>
						<a href="?view=proses-barang&gudang=Hapus&KodeBarang=<?php echo $u['KodeBarang'] ?>" class="btn btn-danger"><i class="ti-trash"></i>&nbsp;&nbsp;Hapus</a>
					<?php } } ?>
				</td>
				<?php } ?>
			</tr>
<?php } ?>
</table>
</div>
</div>

<!-- TAMPILAN DETAIL BARANG -->

<?php } elseif($_GET['tampil']=='detail'){

	if(isset($_SESSION['u_barang'])){
		echo '<div class="alert alert-warning">Gagal Mengubah Data Barang..!!</div>';
		unset($_SESSION['u_barang']);
	}

	$z=mysqli_query($con,"SELECT * FROM brg WHERE KodeBarang='$_GET[KodeBarang]'");
	$v=mysqli_fetch_array($z); ?>

<div class="animated fadeIn">
	<div class="card">
		<form method="POST" action="?view=proses-barang&gudang=Ubah" enctype="multipart/form-data">
			<div class="card-header">
			    <center><img width="400" height="200" src="images/<?php echo $v['Gambar']?>" id="ubh_gbr">
			    	<br><p></p>
			    	<input type="file" class="form-control" name="Gambar" hidden="" id="Gambar" id="formgambar" onchange="keluaran(event)">
			    	<button class="btn btn-outline-primary col-4" id="btn_Gambar" type="button">Ubah Gambar</button>
				</center>
			</div><br>
			<table class="table table-striped table-bordered">
				<tr>
					<td><b>Kode Barang</b></td>
					<td><input class="form-control" name="KodeBarang" value="<?php echo $v['KodeBarang']; ?>" readonly></td>
				</tr>
				<tr>
					<td><b>Nama Barang</b></td>
					<td><input class="form-control" name="NamaBarang" value="<?php echo $v['NamaBarang']; ?>"></td>
				</tr>
				<tr>
					<td><b>Stok Barang</b></td>
					<td><input class="form-control" value="<?php echo $v['StokGudang']; ?>" readonly=""></td>
				</tr>
			</table>
			<div class="card-footer"><br>
				<div style="float: left;">
					<button class="btn btn-primary">Ubah Data Barang</button>
				</div>
				<div style="float: right;">
					<a style="width: 150px;" href="index.php?view=barang">
						<button class="btn btn-dark" type="button"><i class="ti-arrow-left"></i> Kembali/Batal</button>
					</a>
				</div>
			</div>
		</form><br>
	</div>
</div>

<script>
	const Gambar = document.getElementById("Gambar");
	const btn_Gambar = document.getElementById("btn_Gambar");
	btn_Gambar.addEventListener("click", function(){
		Gambar.click();
	});

	let keluaran = function(event){
		const ubh_gbr = document.getElementById("ubh_gbr");
		ubh_gbr.src = URL.createObjectURL(event.target.files[0]);

	}
</script>

<?php } ?>

<script>
	const a = document.getElementById("btn_cari");
	a.onclick = function(){
		const b = document.getElementById("cari").value;
		if(b == ''){
			alert('Isikan Form Pencarian Dulu..');
		}
		else{
			location.href='index.php?view=barang&cari='+b
		}
	}
</script>

