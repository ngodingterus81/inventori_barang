<div class="animated fadeIn">
	<div class="card">
<?php 
	if($_GET['tampil']=='' || $_GET['tampil']=='Data'){ ?>

		<div class="card-header">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-5" style="width:100%;float:left;">
						<h3><strong>Data Pengiriman Barang</strong></h3>
					</div>
					<div class="col-md-5" style="width:100%;float:right;">
						<input type="text" class="form-control col-md-12" id="cari" placeholder="Pencarian Berdasarkan No. Surat">	
					</div>
					<div class="col-md-1" style="width:100%;float:right;">
						<button class="col-md-12 btn btn-warning" onclick="cari($('#cari').val())" type="button">Cari</button>
					</div>
					<div class="col-md-1" style="width:100%;float:right;">
						<a href="?view=g_permintaan_barang&tampil=Data" class="btn btn-primary">Reset</a>
					</div>
				</div>
			</div>
		</div><br><?php if($_SESSION['level']=='Gudang'){ ?>
		<div class="col-md-4">
			<a href="?view=g_permintaan_barang&tampil=Tambah" class="btn btn-success">Buat Pengiriman</a>
		</div>
		<br><?php } ?>
		<div class="table-responsive">
			<table class="table table-striped table-bordered text-center">
				<thead>
					<tr style="font-size: 12px;">
						<th>No.</th>
						<th>Kode Surat<br>Tgl Surat</th>
						<th>Asal Tugas<br>Nama Petugas</th>
						<th>Nama Pengirim</th>
						<th>Alamat Pengirim</th>
						<th>Tgl Kirim<br>Tgl Terima</th>
						<th>Kode Barang<br>Nama Barang</th>
						<th>Jumlah Beli<br>Harga</th>
						<th>Total</th>
						<?php if($_SESSION['level']=='Gudang'){ ?>
						<th>Kode Kurir<br>Nama Kurir</th>
						<?php }elseif($_SESSION['level']=='Admin'){ ?>
						<th>Kode Petugas<br>Nama Petugas</th>
						<?php } ?>
						<th>Status</th><?php if($_SESSION['level']=='Gudang'){ ?>
						<th>Aksi</th><?php } ?>
					</tr>
				</thead>
				<tbody>
					<?php
					if(isset($_GET['Cari'])){
						if($_SESSION['level']=='Kurir'){
							
							$data = mysqli_query($con,"SELECT a.SJGudang, a.Tgl_SuratTugas, a.AsalTugas, a.NamaPetugas, a.NamaPemesan, a.AlamatPemesan, a.TglKirim, a.TglTerima, a.KodeBarang, b.NamaBarang, a.JumlahBeli, a.HargaBarang, a.Total, a.KodeKurir, c.nm_pegawai AS NmKurir, a.KodePetugas, d.nm_pegawai AS NmPetugas, a.Status FROM g_kirim a JOIN brg b ON a.KodeBarang=b.KodeBarang JOIN pegawai c ON a.KodeKurir=c.id_pegawai WHERE a.KodeKurir= '$_SESSION[id]' AND a.SJGudang LIKE '%$_GET[Cari]%' JOIN pegawai b ON a.KodePetugas=d.id_pegawai ORDER BY a.Tgl_SuratTugas DESC");
						}else{

							$data = mysqli_query($con,"SELECT a.SJGudang, a.Tgl_SuratTugas, a.AsalTugas, a.NamaPetugas, a.NamaPemesan, a.AlamatPemesan, a.TglKirim, a.TglTerima, a.KodeBarang, b.NamaBarang, a.JumlahBeli, a.HargaBarang, a.Total, a.KodeKurir, c.nm_pegawai AS NmKurir, a.KodePetugas, d.nm_pegawai AS NmPetugas, a.Status FROM g_kirim a JOIN brg b ON a.KodeBarang=b.KodeBarang JOIN pegawai c ON a.KodeKurir=c.id_pegawai WHERE a.SJGudang LIKE '%$_GET[Cari]%' JOIN pegawai b ON a.KodePetugas=d.id_pegawai ORDER BY a.Tgl_SuratTugas DESC");
					}}
					else{
						if($_SESSION['level']=='Kurir'){
							$data = mysqli_query($con,"SELECT a.SJGudang, a.Tgl_SuratTugas, a.AsalTugas, a.NamaPetugas, a.NamaPemesan, a.AlamatPemesan, a.TglKirim, a.TglTerima, a.KodeBarang, b.NamaBarang, a.JumlahBeli, a.HargaBarang, a.Total, a.KodeKurir, c.nm_pegawai AS NmKurir, a.KodePetugas, d.nm_pegawai AS NmPetugas, a.Status FROM g_kirim a JOIN brg b ON a.KodeBarang=b.KodeBarang JOIN pegawai c ON a.KodeKurir=c.id_pegawai WHERE a.KodeKurir = '$_SESSION[id]' JOIN pegawai b ON a.KodePetugas=d.id_pegawai ORDER BY a.Tgl_SuratTugas DESC");
						}else{

							$data = mysqli_query($con,"SELECT a.SJGudang, a.Tgl_SuratTugas, a.AsalTugas, a.NamaPetugas, a.NamaPemesan, a.AlamatPemesan, a.TglKirim, a.TglTerima, a.KodeBarang, b.NamaBarang, a.JumlahBeli, a.HargaBarang, a.Total, a.KodeKurir, c.nm_pegawai AS NmKurir, a.KodePetugas, d.nm_pegawai AS NmPetugas, a.Status FROM g_kirim a JOIN brg b ON a.KodeBarang=b.KodeBarang JOIN pegawai c ON a.KodeKurir=c.id_pegawai JOIN pegawai d ON a.KodePetugas=d.id_pegawai ORDER BY a.Tgl_SuratTugas DESC");
					}}
						$cek = mysqli_num_rows($data);
						if($cek > 0){
							$a = 1;
							while($lihat = mysqli_fetch_array($data)){ ?>
							<tr style="font-size: 12px;">
								<td><p></p><?=$a++;?></td>
								<td><?=$lihat['SJGudang']?><hr><?=$lihat['Tgl_SuratTugas']?></td>
								<td><?=$lihat['AsalTugas']?><hr><?=$lihat['NamaPetugas']?></td>
								<td><p></p><?=$lihat['NamaPemesan']?></td>
								<td><p></p><?=$lihat['AlamatPemesan']?></td>
								<td><?=$lihat['TglKirim']?><hr><?=$lihat['TglTerima']?></td>
								<td><?=$lihat['KodeBarang']?><hr><?=$lihat['NamaBarang']?></td>
								<td><?=$lihat['JumlahBeli']?><hr><?=$lihat['HargaBarang']?></td>
								<td><p></p>Rp. <?=$lihat['Total']?></td>
								<td>
									<?php if($_SESSION['level']=='Gudang'){
										echo $lihat['KodeKurir'].'<hr>'.$lihat['NmKurir'];
										}else{
											echo $lihat['KodePetugas'].'<hr>'.$lihat['NmPetugas'];
										}
									?>
								</td>
								<td <?php if($_SESSION['level']=='Admin'){ ?>style="font-size: 12px;" <?php }else{?> style="font-size: 10px;" <?php } ?>>
									<?php if($_SESSION['level']=='Gudang'){ ?>
									<?=$lihat['Status']?>
									<?php }elseif($_SESSION['level']=='Kurir'){
										if($lihat['Status'] == 'Kirim'){ ?>
											<a href="?view=kirim&kurir=proses&Kode=<?=$lihat['SJGudang']?>"><button class="btn btn-sm btn-primary">Proses</button></a><?php
										}else{ ?>
											<button class="btn btn-sm btn-dark">Sukses</button>	<?php
										}
									}else{ 
										if($lihat['Status'] == 'Menunggu'){ ?>
										<button class="btn btn-warning btn-sm" onclick="konfir('<?=$lihat[SJGudang]?>')">Konfirmasi</button><br><p></p>
										<button class="btn btn-danger btn-sm" onclick="cancel('<?=$lihat[SJGudang]?>')">Cancel</button>
									<?php }else{?> <button class="btn btn-success btn-sm disabled">Sukses</button><?php }} ?>
								</td><?php if($_SESSION['level']=='Gudang'){ ?>
								<td>
									<?php if($lihat['Status']=='Menunggu'){ ?>
										<?php if($_SESSION['id']==$lihat['KodePetugas']){ ?>
										<a href="?view=g_permintaan_barang&tampil=Ubah&Kode=<?=$lihat['SJGudang']?>">
											<button class="btn-sm btn btn-primary" style="font-size: 12px;">Ubah</button>
										</a>
										<br><p></p>
										<button class="btn-sm btn btn-danger" onclick="hapus('<?=$lihat[SJGudang]?>')" style="font-size: 12px;">Hapus</button><?php }else{echo "<button class='btn btn-dark btn-sm disabled'>No Aksi</button>";} ?>
									<?php }else{ ?>
										<button class="btn-sm btn btn-dark disabled">No Aksi</button>
									<?php } ?>
								</td><?php } ?>
							</tr>
						<?php }}
						else{
							echo "<tr><td colspan='10'><h3>Tidak Menemukan Data Apapun</h3></td></tr>";
						} ?>
				</tbody>
			</table>
		</div>

<?php 	}

	elseif($_GET['tampil']=='Tambah'){ ?>
	<?php
		if(isset($_SESSION['tgambar'])){
			if($_SESSION['tgambar']=='Kosong'){ ?>
				<div class="alert alert-warning"><h4>Di Wajibkan Untuk Mengisi Gambar Surat Sebagai Bukti Permintaan Barang..!</h4></div>
	<?php	unset($_SESSION['tgambar']);}
		} ?>
		<div class="card-header">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12 text-center">
						<h3><strong>Form Permintaan Barang</strong></h3>
					</div>
				</div>
			</div>
		</div><br>
		<form action="?view=proses-barang&gudang=Kirim" method="POST" enctype="multipart/form-data">
			<div class="card-body">
				<div class="row">
					<div class="col-12">
						<div class="row">
							<div class="col-md-3">
								<label for="KodeSurat"><b>Kode Surat Tugas</b></label>
								<input required type="text" class="form-control" name="KodeSurat" id="KodeSurat" required="">
							</div>
							<div class="col-md-3">
								<label for="TglSurat"><b>Tanggal Surat Tugas</b></label>
								<input required type="date" class="form-control" name="TglSurat" id="TglSurat" required="">
							</div>
							<div class="col-md-3">
								<label for="AsalSurat"><b>Asal Surat Tugas</b></label>
								<input required type="text" class="form-control" name="AsalSurat" id="AsalSurat" required="">
							</div>
							<div class="col-md-3">
								<label for="NamaPetugas"><b>Nama Petugas</b></label>
								<input required type="text" class="form-control" name="NamaPetugas" id="NamaPetugas" required="">
							</div>
						</div><br>
						<div class="row">
							<div class="col-md-4">
								<label for="TglKirim"><b>Tanggal Kirim</b></label>
								<input required type="date" class="form-control" name="TglKirim" id="TglKirim" required="">
							</div>
							<div class="col-md-4">
								<label for="NamaPemesan"><b>Nama Pemesan</b></label>
								<input required type="text" class="form-control" name="NamaPemesan" id="NamaPemesan" required="">
							</div>
							<div class="col-md-4">
								<label for="AlamatPemesan"><b>Alamat Pemesan</b></label>
								<input required type="text" class="form-control" name="AlamatPemesan" id="AlamatPemesan" required="">
							</div>
						</div><br>
						<div class="row">
							<div class="col-md-3">
								<label for="KodeBarang"><b>Kode Barang</b></label>
								<select name="KodeBarang" id="KodeBarang" class="form-control">
									<option value="" hidden="">Pilih Kode Barang</option>
									<?php $data = mysqli_query($con,"SELECT * FROM brg");while($lihat = mysqli_fetch_array($data)){ ?>
										<option value="<?=$lihat['KodeBarang']?>"><?=$lihat['KodeBarang']?></option>
									<?php } ?>
								</select>
							</div>
							<div class="col-md-3" id="formNamaBarang">
								<label for="NamaBarang"><b>Nama Barang</b></label>
								<input required type="text" class="form-control" name="NamaBarang" id="NamaBarang" readonly="">
							</div>
							<div class="col-md-3">
								<label for="HargaBarang"><b>Harga Barang</b></label>
								<input required type="text" class="form-control" name="HargaBarang" id="HargaBarang">
							</div>
							<div class="col-md-3">
								<label for="JumlahBeli"><b>Jumlah Beli</b></label>
								<input required type="number" class="form-control" name="JumlahBeli" id="JumlahBeli">
							</div>
						</div><br>
						<div class="row">
							<div class="col-md-4" id="formTotal">
								<label for="Total"><b>Total Belanjaan</b></label>
								<input required type="number" class="form-control" name="Total" id="Total" readonly="">
							</div>
							<div class="col-md-4">
								<label for="UserKurir"><b>Pilih Kurir</b></label>
									<select name="UserKurir" id="UserKurir" class="form-control" required="">
										<option value="" hidden="">--Pilih Kurir--</option>
									<?php $result = mysqli_query($con,"SELECT * FROM pegawai WHERE bagian = 'Kurir'");while($lihat=mysqli_fetch_array($result)){?>
										<option value="<?=$lihat['id_pegawai']?>"><?=$lihat['nm_pegawai']?></option>
									<?php } ?>
									</select>
							</div>
							<div class="col-md-4">
								<label for="UserGudang"><b>Petugas Gudang</b></label>
								<input type="text" hidden="" readonly="" name="UserGudang" id="UserGudang" value="<?=$_SESSION['id']?>">
								<input type="text" class="form-control" name="namauser" id="namauser" readonly="" value="<?=$_SESSION['nama']?>">
							</div>
						</div><br>
						<div class="row">
							<div class="col-md-12">
								<label for="Deskripsi"><b>Deskripsi Pengiriman Barang</b></label>
								<textarea name="Deskripsi" id="Deskripsi" cols="10" rows="2" class="form-control"></textarea>
							</div>
						</div><br>
						<div class="row">
							<div class="col-md-12">
								<label for="Bukti"><b>Upload Bukti Surat Tugas</b></label>
								<input type="file" class="form-control" name="Bukti" id="Bukti" required="">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="card-footer">
				<button class="btn btn-success">Buat Pengiriman</button>
				&nbsp;&nbsp;&nbsp;
				<a href="?view=g_permintaan_barang&tampil=Data"><button type="button" class="btn btn-dark">Batal/Kembali</button></a>
			</div>

		</form>

<?php	}
	elseif($_GET['tampil']=='Ubah'){ 
		$result = mysqli_query($con,"SELECT * FROM g_kirim a JOIN brg b ON a.KodeBarang=b.KodeBarang JOIN pegawai c ON a.KodeKurir=c.id_pegawai WHERE a.SJGudang='$_GET[Kode]'");
		$d = mysqli_fetch_array($result);
		if($_SESSION['id']==$d['KodePetugas']){
?>

			<div class="card-header">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-12 text-center">
							<h3><strong>Form Ubah Permintaan Barang</strong></h3>
						</div>
					</div>
				</div>
			</div><br>
			<form action="?view=proses-barang&gudang=UbahKirim" method="POST" enctype="multipart/form-data">
				<div class="card-body">
					<div class="row">
						<div class="col-12">
							<div class="row">
								<div class="col-md-3">
									<label for="KodeSurat"><b>Kode Surat Tugas</b></label>
									<input  type="text" class="form-control" name="KodeSurat" id="KodeSurat" value="<?=$d['SJGudang']?>" readonly="">
								</div>
								<div class="col-md-3">
									<label for="TglSurat"><b>Tanggal Surat Tugas</b></label>
									<input  type="date" class="form-control" name="TglSurat" id="TglSurat" value="<?=$d['Tgl_SuratTugas']?>">
								</div>
								<div class="col-md-3">
									<label for="AsalSurat"><b>Asal Surat Tugas</b></label>
									<input  type="text" class="form-control" name="AsalSurat" id="AsalSurat" value="<?=$d['AsalTugas']?>">
								</div>
								<div class="col-md-3">
									<label for="NamaPetugas"><b>Nama Petugas</b></label>
									<input  type="text" class="form-control" name="NamaPetugas" id="NamaPetugas" value="<?=$d['NamaPetugas']?>">
								</div>
							</div><br>
							<div class="row">
								<div class="col-md-4">
									<label for="TglKirim"><b>Tanggal Kirim</b></label>
									<input  type="date" class="form-control" name="TglKirim" id="TglKirim" value="<?=$d['TglKirim']?>">
								</div>
								<div class="col-md-4">
									<label for="NamaPemesan"><b>Nama Pemesan</b></label>
									<input  type="text" class="form-control" name="NamaPemesan" id="NamaPemesan" value="<?=$d['NamaPemesan']?>">
								</div>
								<div class="col-md-4">
									<label for="AlamatPemesan"><b>Alamat Pemesan</b></label>
									<input  type="text" class="form-control" name="AlamatPemesan" id="AlamatPemesan" value="<?=$d['AlamatPemesan']?>">
								</div>
							</div><br>
							<div class="row">
								<div class="col-md-3">
									<label for="KodeBarang"><b>Kode Barang</b></label>
									<select name="KodeBarang" id="KodeBarang" class="form-control">
										<option value="<?=$d['KodeBarang']?>" hidden=""><?=$d['KodeBarang']?></option>
										<?php $data = mysqli_query($con,"SELECT * FROM brg");while($lihat = mysqli_fetch_array($data)){ ?>
											<option value="<?=$lihat['KodeBarang']?>"><?=$lihat['KodeBarang']?></option>
										<?php } ?>
									</select>
								</div>
								<div class="col-md-3" id="formNamaBarang">
									<label for="NamaBarang"><b>Nama Barang</b></label>
									<input  type="text" class="form-control" name="NamaBarang" id="NamaBarang" readonly="" value="<?=$d['NamaBarang']?>">
								</div>
								<div class="col-md-3">
									<label for="HargaBarang"><b>Harga Barang</b></label>
									<input  type="text" class="form-control" name="HargaBarang" id="HargaBarang" value="<?=$d['HargaBarang']?>">
								</div>
								<div class="col-md-3">
									<label for="JumlahBeli"><b>Jumlah Beli</b></label>
									<input  type="number" class="form-control" name="JumlahBeli" id="JumlahBeli" value="<?=$d['JumlahBeli']?>">
								</div>
							</div><br>
							<div class="row">
								<div class="col-md-4" id="formTotal">
									<label for="Total"><b>Total Belanjaan</b></label>
									<input type="number" class="form-control" name="Total" id="Total" readonly="" value="<?=$d['Total']?>">
								</div>
								<div class="col-md-4">
									<label for="UserKurir"><b>Pilih Kurir</b></label>
										<select name="UserKurir" id="UserKurir" class="form-control">
											<option value="<?=$d['KodeKurir']?>" hidden=""><?=$d['nm_pegawai']?></option>
									<?php $result = mysqli_query($con,"SELECT * FROM pegawai WHERE bagian = 'Kurir'");while($lihat=mysqli_fetch_array($result)){?>
											<option value="<?=$lihat['id_pegawai']?>"><?=$lihat['nm_pegawai']?></option>
									<?php } ?>
										</select>
								</div>
								<div class="col-md-4">
									<label for="UserGudang"><b>Petugas Gudang</b></label>
									<input type="text" hidden="" readonly="" name="UserGudang" id="UserGudang" value="<?=$_SESSION['id']?>">
									<input type="text" class="form-control" name="namauser" id="namauser" readonly="" value="<?=$_SESSION['nama']?>">
								</div>
							</div><br>
							<div class="row">
								<div class="col-md-12">
									<label for="Deskripsi"><b>Deskripsi Pengiriman Barang</b></label>
									<textarea name="Deskripsi" id="Deskripsi" cols="10" rows="2" class="form-control"><?=$d['Deskripsi']?></textarea>
								</div>
							</div><br>
							<div class="row">
								<div class="col-md-12">
									<label for="Bukti"><b>Upload Bukti Surat Tugas</b></label>
									<input type="file" class="form-control" name="Bukti" id="Bukti">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card-footer">
					<button class="btn btn-success">Ubah Data Pengiriman</button>
					&nbsp;&nbsp;&nbsp;
					<a href="?view=g_permintaan_barang&tampil=Data"><button type="button" class="btn btn-dark">Batal/Kembali</button></a>
				</div>

			</form>

<?php	}else{
	echo "<br>
		<center>
			<h3><b>Maaf Ini Bukan Hak Akses Anda..!!</b></h3>
			<br><br>
			<a href='?view=g_permintaan_barang&tampil=Data'>
				<button class='btn btn-warning'><h6><b><i class='fa fa-reply'></i> Kembali</b></h6></button>
			</a>
		</center>
		<br>";
}}
?>

	</div>
</div>

<script>
	const KodeBarang = document.getElementById("KodeBarang");
	KodeBarang.addEventListener("input", function(){		
		$.ajax({
			type : "POST",
			data : "Kode="+KodeBarang.value,
			url : "gudang/form.php?form=namabarang",
			success : function(response){
				let formNamaBarang = $("#formNamaBarang");
				formNamaBarang.html(response);
			}
		});
	})
	const JumlahBeli = document.getElementById("JumlahBeli");
	const HargaBarang = document.getElementById("HargaBarang");
	JumlahBeli.addEventListener("input", function(){
		if(HargaBarang == ''){
			alert("Mohon Untuk Mengisi Form Harga Barang Terlebih Dahulu..");
		}
		else{
			$.ajax({
				type : "POST",
				data : "Harga="+HargaBarang.value+"&JumlahBeli="+JumlahBeli.value,
				url : "gudang/form.php?form=harga",
				success : function(response){
					let formTotal = $("#formTotal");
					formTotal.html(response);
				}
			});
		}
	});

	HargaBarang.addEventListener("input", function(){
		if(HargaBarang == ''){
			alert("Mohon Untuk Mengisi Form Harga Barang Terlebih Dahulu..");
		}
		else{
			$.ajax({
				type : "POST",
				data : "Harga="+HargaBarang.value+"&JumlahBeli="+JumlahBeli.value,
				url : "gudang/form.php?form=harga",
				success : function(response){
					let formTotal = $("#formTotal");
					formTotal.html(response);
				}
			});
		}
	});

	function total(){
		const JumlahBeli = document.getElementById("JumlahBeli");
		const HargaBarang = document.getElementById("HargaBarang");
		$.ajax({
				type : "POST",
				data : "Harga="+HargaBarang.value+"&JumlahBeli="+JumlahBeli.value,
				url : "gudang/form.php?form=harga",
				success : function(response){
					let formTotal = $("#formTotal");
					formTotal.html(response);
				}
			});
	}
	function hapus(Kode){
		let konfirmasi = confirm("Yakin Akan Menghapus Data Ini..!");
		if(konfirmasi == true){
			location.href='?view=proses-barang&gudang=HapusKirim&Kode='+Kode;
		}
	}
	function konfir(Kode){
		location.href='?view=proses-barang&gudang=KonfirKirim&Kode='+Kode;
	}
	function cancel(Kode){
		location.href='?view=proses-barang&gudang=HapusKirim&Kode='+Kode;
	}

	function cari(carikey){
		if (carikey == "") {
			alert("Form Pencarian Masih Kosong..!!");
		}
		else{
			location.href='?view=g_permintaan_barang&tampil=Data&Cari='+carikey;
		}
	}
</script>