<div class="animated fadeIn">
	<div class="card">
<div class="card-header">
	<center><h3><strong>Laporan Barang Masuk</strong><h3></center>
</div>
<br/>
<form action="cetak_brg_baru.php" method="POST" target="_blank">
	<label>Dari :</label><input type="date" name="dari">
	<label>Sampai :</label><input type="date" name="sampai">
	<button type="submit" class="btn btn-primary" name="cetak"><i class="fa fa-print"></i>&nbsp;&nbsp;Cetak</button>
</form><br/>
<table class="table table-striped table-bordered">
<tr>
	<th>No</th>
	<th>Nama Barang</th>
	<th>Tanggal Masuk</th>
	<th>Jumlah Beli</th>
	<th>Harga</th>
	<th>Biaya Kirim</th>
	<th>Pengirim</th>
	<th>Penerima</th>
	<th>Total</th>
</tr>


<?php
include"koneksi.php";

$c=mysqli_query($con,"select * from brg_msk");
	$no=1;
while($u=mysqli_fetch_array($c)){
	$id_brg = $u['id_brg'];
?>
<tr>
	<td><?php echo $no++ ?></td>
	<td><?php echo $u['nm_brg'] ?></td>
	<td><?php echo date('d F Y', strtotime($u['tgl_msk'])) ?></td>
	<td><?php echo $u['jml_beli'] ?></td>
	<td>RP. <?php echo number_format($u['hrg_beli']) ?></td>
	<td>RP. <?php echo number_format($u['biaya_kirim']) ?></td>
	<td><?php echo $u['pengirim'] ?></td>
	<td><?php echo $u['penerima'] ?></td>
	<td>RP. <?php echo number_format($u['total']) ?></td>
</tr>
<?php } ?>
</table>
</div>
</div>