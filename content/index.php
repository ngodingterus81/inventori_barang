<?php
session_start();
error_reporting(0);
include"koneksi.php";
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Barang Masuk Dan Keluar</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">

    <link rel="stylesheet" href="vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="vendors/selectFX/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="vendors/jqvmap/dist/jqvmap.min.css">
    <link rel="stylesheet" href="vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">


    <link rel="stylesheet" href="assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <script src="vendors/jquery/dist/jquery.min.js"></script>

</head>

<body>
    <?php include"navigasi.php"; 

    if(@$_GET['view']==''){
          include"dashboard.php";
    }
    elseif($_GET['view']=='dashboard'){
          include"dashboard.php";
    }
    //PRODUKSI
    elseif($_GET['view']=='p_masuk-barang-lama'){
        include"produksi/masuk-barang-lama.php";
    }
    elseif($_GET['view']=='p_pengajuan_barang'){
        include"produksi/pengajuan-barang.php";
    }
    elseif($_GET['view']=='proses-produksi'){
        include"produksi/proses-produksi.php";
    }
    elseif($_GET['view']=='K_produksi_barang'){
        include"produksi/konfirmasi-barang.php";
    }
    elseif($_GET['view']=='ubah_masuk_barang'){
        include"produksi/ubah-masuk-barang.php";
    }

    //BARANG
    elseif($_GET['view']=='barang'){
        include"gudang/barang.php";
    }
    elseif($_GET['view']=='proses-barang'){
        include"proses-barang.php";
    }
	
    //PEGAWAI
    elseif($_GET['view']=='pegawai'){
        include"admin/pegawai.php";
    }

    //GUDANG
    elseif($_GET['view']=='g_permintaan_barang'){
        include"gudang/permintaan-barang.php";
    }

    //KURIR
    elseif($_GET['view']=='kirim'){
        include"kurir/pengiriman.php";
    }
		  
    elseif($_GET['view']=='input_brg_msk'){
        include"input_brg_msk.php";
    }
		  
    elseif($_GET['view']=='brg_msk'){
        include"brg_msk.php";
    }
		  
    elseif($_GET['view']=='input_pegawai'){
        include"input_pegawai.php";
    }
    elseif($_GET['view']=='edit_pegawai'){
        include"edit_pegawai.php";
    }
		  
    elseif($_GET['view']=='det_brg'){
        include"det_brg.php";
    }
		  
    elseif($_GET['view']=='input_brg_lama'){
        include"input_brg_lama.php";
    }
		  
    elseif($_GET['view']=='lap_mohon'){
        include"lap_mohon.php";
    }
		  
    elseif($_GET['view']=='kirim_mohon'){
        include"kirim_mohon.php";
    }
		  
    elseif($_GET['view']=='lap_mohon_baru'){
        include"lap_mohon_baru.php";
    }
		  
    elseif($_GET['view']=='tampil_brg_baru'){
        include"tampil_brg_baru.php";
    }
		  
    elseif($_GET['view']=='notif'){
        include"notif.php";
    }
		  
    elseif($_GET['view']=='persetujuan'){
        include"persetujuan.php";
    }
		  
    elseif($_GET['view']=='konfirmasi'){
        include"konfirmasi.php";
    }
		  
    elseif($_GET['view']=='notiff'){
        include"notiff.php";
    }
		  
    elseif($_GET['view']=='ok'){
        include"ok.php";
    }
		  
    elseif($_GET['view']=='persetujuann'){
        include"persetujuann.php";
    }
		  
    elseif($_GET['view']=='edit'){
        include"edit.php";
    }
		  
    elseif($_GET['view']=='okk'){
        include"okk.php";
    }
		  
    elseif($_GET['view']=='brg_klr'){
        include"brg_klr.php";
    }
    elseif($_GET['view']=='kirim'){
        include"kirim.php";
    }
		  
    elseif($_GET['view']=='notifff'){
        include"notifff.php";
    }
		  
    elseif($_GET['view']=='brg_kirim'){
        include"brg_kirim.php";
    }
		  
    elseif($_GET['view']=='terima'){
        include"terima.php";
    }
		  
    elseif($_GET['view']=='lap_brg_msk'){
        include"lap_brg_msk.php";
    }
		  
    elseif($_GET['view']=='lap_brg_klr'){
        include"lap_brg_klr.php";
    }
		  
    elseif($_GET['view']=='hapus_brg'){
        include"hapus_brg.php";
    }
		  
    elseif($_GET['view']=='ajax'){
        include"ajax.php";
    }
    elseif($_GET['view']=='edit_brg_klr'){
        include"edit_brg_klr.php";
    }
		  
    elseif($_GET['view']=='hapus_brg_klr'){
        include"hapus_brg_klr.php";
    }
		  
    elseif($_GET['view']=='tolak_kirim'){
        include"tolak_kirim.php";
    }
		  
    elseif($_GET['view']=='hapus'){
        include"hapus.php";
    }
		  
    elseif($_GET['view']=='lap_brg'){
        include"lap_brg.php";
    }
		  
    elseif($_GET['view']=='brg'){
        include"brg.php";
    }
		  
    elseif($_GET['view']=='brg_terima'){
        include"brg_terima.php";
    }
		  
    elseif($_GET['view']=='co'){
        include"co.php";
    }
       ?>

       <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="scrollmodalLabel">Pilih Barang</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Kode Barang</th>
                                        <th>Nama Barang</th>
                                        <th>Stok Barang</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $u=mysql_query("select * from brg");$a=1;
                                        while($j=mysql_fetch_array($u)){ ?>
                                        <tr>
                                            <td><?=$a;?></td>
                                            <td><a href="#KodeBarang=<?=$j['KodeBarang'];?>"><?=$j['KodeBarang'];?></a></td>
                                            <td><?=$j['NamaBarang'];?></td>
                                            <td><?=$j['StokGudang'];?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

    <!-- Right Panel -->

    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="assets/js/main.js"></script>

    <script src="vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="vendors/jszip/dist/jszip.min.js"></script>
    <script src="vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.colVis.min.js"></script>
    <script src="assets/js/init-scripts/data-table/datatables-init.js"></script>

    <script src="vendors/chart.js/dist/Chart.bundle.min.js"></script>
    <script src="assets/js/dashboard.js"></script>
    <script src="assets/js/widgets.js"></script>
    <script src="vendors/jqvmap/dist/jquery.vmap.min.js"></script>
    <script src="vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <script src="vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script>
        (function($) {
            "use strict";

            jQuery('#vmap').vectorMap({
                map: 'world_en',
                backgroundColor: null,
                color: '#ffffff',
                hoverOpacity: 0.7,
                selectedColor: '#1de9b6',
                enableZoom: true,
                showTooltip: true,
                values: sample_data,
                scaleColors: ['#1de9b6', '#03a9f5'],
                normalizeFunction: 'polynomial'
            });
        })(jQuery);
    </script>

</body>

</html>
