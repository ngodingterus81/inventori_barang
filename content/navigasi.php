<aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="./"><?=$_SESSION['level']?></a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="?view=notif"> <i class="menu-icon fa fa-dashboard"></i>Halaman Awal </a>
                    </li>
                    <h3 class="menu-title">Navigasi Menu <?=$_SESSION['level'] ?></h3><!-- /.menu-title -->
                    <?php if($_SESSION['level']=='Produksi' || $_SESSION['level']=='Gudang'){ ?>
                        <li><a href="?view=barang"><i class="menu-icon fa fa-puzzle-piece"></i>Data  Barang</a></li>
                    <?php } if($_SESSION['level']=='Admin'){ ?>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="menu-icon fa fa-laptop"></i> Data Master
                        </a>
                        <ul class="sub-menu children dropdown-menu">
                            <?php if($_SESSION['level']=='Admin'){ ?>
                                <li><i class="fa fa-puzzle-piece"></i><a href="?view=pegawai&tampil=Data">Pegawai</a></li>
                                <li><i class="fa fa-puzzle-piece"></i><a href="?view=barang">Data Barang</a></li>
                            <?php } ?>
                        </ul>
                    </li>
                    <?php } ?>
                        <?php if($_SESSION['level']=='Gudang' || $_SESSION['level']=='Produksi'){ ?>
                        <li><a href="?view=p_pengajuan_barang"><i class="menu-icon fa fa-puzzle-piece"></i>Pengajuan Barang</a></li>
                        <?php }if($_SESSION['level']=='Gudang' || $_SESSION['level']=='Admin'){ ?>
                        <li><a href="?view=g_permintaan_barang&tampil=Data"><i class="menu-icon fa fa-puzzle-piece"></i>Permintaan Barang</a></li>
                    <?php } if($_SESSION['level']=='Kurir'){ ?>
                    <li><a href="?view=g_permintaan_barang&tampil=Data"><i class="menu-icon fa fa-puzzle-piece"></i> Pengiriman Barang</a></li>
                    <?php } if($_SESSION['level']!='Kurir'){?>
                    <li class="menu-item-has-children dropdown">
                    	 <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-tasks"></i>Laporan</a>
                        <ul class="sub-menu children dropdown-menu">
                        	 <li><i class="fa fa-puzzle-piece"></i><a href="?view=lap_brg_msk">Data Barang</a></li>
                             <?php if($_SESSION['level']=='Gudang' || $_SESSION['level']=='Produksi'){ ?>
                             <li><i class="fa fa-puzzle-piece"></i><a href="?view=lap_brg_klr">Pengajuan Barang</a></li>
                             <?php }if($_SESSION['level']=='Gudang' || $_SESSION['level']=='Admin') { ?>
                             <li><i class="fa fa-puzzle-piece"></i><a href="?view=lap_brg">Permintaan Barang</a></li>
                            <?php } ?>
                         </ul>

                    </li>
                    <?php } ?>
                    <li><a href="logout.php"> <i class="menu-icon fa fa-sign-out"></i>Logout</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->