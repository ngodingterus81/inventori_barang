<?php if($_GET['tampil']=='Data') { ?>
	<div class="animated fadeIn">
		<div class="card">
			<div class="card-header">
			    <center><h3><strong>Master Pegawai</strong><h3></center>
			</div>
			&nbsp;&nbsp;&nbsp;
			<a href="?view=pegawai&tampil=Tambah"><button class="btn btn-primary"><i class="ti-pencil-alt"></i> Tambah Pegawai</button></a><br/>
			<table class="table table-striped table-bordered">
			<tr>
				<th>No</th>
				<th>ID Pegawai</th>
				<th>Nama Pegawai</th>
				<th>Tgl Lahir</th>
				<th>Jenis Kelamin</th>
				<th>No Telepon</th>
				<th>Bagian</th>
				<th>Alamat</th>
				<th>Opsi</th>
			</tr>


			<?php
			$no=1;
			$c=mysqli_query($con,"SELECT * FROM pegawai ORDER BY bagian ASC, nm_pegawai ASC");
			while($u=mysqli_fetch_array($c)){
			?>
			<tr>
				<td><?php echo $no++ ?></td>
				<td><?php echo $u['id_pegawai'] ?></td>
				<td><?php echo $u['nm_pegawai'] ?></td>
				<td><?php echo $u['tgl_lahir'] ?></td>
				<td><?php echo $u['jenkel'] ?></td>
				<td><?php echo $u['nohp'] ?></td>
				<td><?php echo $u['bagian'] ?></td>
				<td><?php echo $u['alamat'] ?></td>
				<td>
					<?php if($u['bagian']!=='Admin'){ ?>
					<a href="?view=pegawai&tampil=Ubah&id_pegawai=<?php echo $u['id_pegawai'] ?>" class="btn btn-warning"><i class="ti-pencil-alt"></i>&nbsp;&nbsp;Edit</a>
					<a onclick="if(confirm('Apakah anda yakin ingin menghapus data ini ??')){ location.href='?view=pegawai&tampil=Hapus&Kode=<?php echo $u['id_pegawai']; ?>' }" class="btn btn-danger"><i class="ti-trash"></i>&nbsp;&nbsp;Hapus</a>
					<?php }else{echo "<button class='btn btn-dark disabled'>No Opsi</button";} ?>
				</td>
			</tr>
			<?php } ?>
			</table>
		</div>
	</div>
<?php }elseif($_GET['tampil']=='Ubah'){ 
	$z=mysql_query("SELECT * FROM pegawai WHERE id_pegawai='$_GET[id_pegawai]'");
	$x=mysql_fetch_array($z); ?>
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-header">
                    <strong>Input Pegawai</strong>
                </div>
                <form method="POST" enctype="multipart/form-data">
	                <div class="card-body card-block">
	                    <div class="form-group">
	                        <label class=" form-control-label">ID Pegawai</label>
	                        <div class="input-group">
	                            <div class="input-group-addon"><i class="fa fa-lock"></i></div>
	                            <input class="form-control" value="<?php echo $x['id_pegawai']; ?>" type="text" name="id_pegawai" readonly="">
	                        </div>
	                    </div>
	                    <div class="form-group">
	                        <label class=" form-control-label">Nama Pegawai</label>
	                        <div class="input-group">
	                            <div class="input-group-addon"><i class="ti-agenda"></i></div>
	                            <input class="form-control" type="text" name="nm_pegawai" value="<?php echo $x['nm_pegawai'] ?>" placeholder="Nama Pegawai ..">
	                        </div>
	                    </div>
	                    <div class="form-group">
	                        <label class=" form-control-label">Jenis Kelamin</label>&nbsp;&nbsp;&nbsp;&nbsp;
	                          <div class="input-group">
	                            <div class="input-group-addon"><i class="fa fa-bolt"></i></div>
	                            <select class="form-control" name="jenkel">
	                            	<option value="<?=$x['jenkel']?>" hidden=""><?=$x['jenkel']?></option>
	                                <option value="Laki-Laki">Laki-Laki</option>
	                                <option value="Perempuan">Perempuan</option>
	                            </select>
	                        </div>
	                    </div>
	                    <div class="form-group">
	                        <label class=" form-control-label">Tanggal Lahir</label>
	                        <div class="input-group">
	                            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
	                            <input value="<?php echo $x['tgl_lahir'] ?>" class="form-control" type="date" name="tgl_lahir">
	                        </div>
	                    </div>
	                    <div class="form-group">
	                        <label class=" form-control-label">No Handphone</label>
	                        <div class="input-group">
	                            <div class="input-group-addon"><i class="fa fa-phone"></i></div>
	                            <input class="form-control" type="number" name="nohp" value="<?php echo $x['nohp'] ?>">
	                        </div>
	                    </div>
	                    <div class="form-group">
	                        <label class=" form-control-label">Username</label>
	                        <div class="input-group">
	                            <div class="input-group-addon"><i class="ti-agenda"></i></div>
	                            <input class="form-control" type="text" name="username" value="<?php echo $x['username'] ?>">
	                        </div>
	                    </div>
	                    <div class="form-group">
	                        <label class=" form-control-label">Password</label>
	                        <div class="input-group">
	                            <div class="input-group-addon"><i class="ti-agenda"></i></div>
	                            <input class="form-control" type="text" name="password" value="<?php echo $x['password'] ?>">
	                        </div>
	                    </div>
	                     <div class="form-group">
	                        <label class=" form-control-label">Bagian</label>
	                        <div class="input-group">
	                            <div class="input-group-addon"><i class="fa fa-archive"></i></div>
	                            <select class="form-control" name="bagian">
	                            	<option value="<?=$x['bagian']?>" hidden=""><?=$x['bagian']?></option>
	                                <option value="Gudang">Gudang</option>
	                                <option value="Kurir">Kurir</option>
	                                <option value="Produksi">Produksi</option>
	                            </select>
	                        </div>
	                    </div>
	                    <div class="form-group">
	                        <label class=" form-control-label">Alamat</label>
	                        <div class="input-group">
	                            <div class="input-group-addon"><i class="ti-clip"></i></div>
	                            <textarea class="form-control" name="alamat" placeholder="Alamat .."><?=$x['alamat']?></textarea>
	                        </div>
	                    </div>
	                    <button type="submit" name="simpan" class="btn btn-primary"><i class="ti-save"></i>&nbsp;&nbsp;&nbsp;Simpan</button>
	                    <a href="?view=pegawai&tampil=Data" class="btn btn-danger"><i class="ti-arrow-left"></i> Batal</a>
	                </div>
            	</form>
	        </div>
	    </div>	
<?php 
	if(isset($_POST['simpan'])){
        $id_pegawai = $_POST['id_pegawai'];
        $nm_pegawai = $_POST['nm_pegawai'];
        $nohp = $_POST['nohp'];
        $jenkel = $_POST['jenkel'];
        $tgl_lahir = $_POST['tgl_lahir'];
        $alamat = $_POST['alamat'];
        $username = $_POST['username'];
        $password = $_POST['password'];
        $bagian = $_POST['bagian'];

        mysql_query("UPDATE pegawai SET 
        	jenkel = '$jenkel', 
        		nohp = '$nohp', 
        			tgl_lahir = '$tgl_lahir', 
        				alamat = '$alamat', 
        					nm_pegawai = '$nm_pegawai', 
        						username = '$username',
        							password = '$password',
        								bagian = '$bagian'
        	WHERE id_pegawai = '$id_pegawai'");
        echo"<script>alert ('Data Berhasil Disimpan')</script>";
        echo"<meta http-equiv='refresh' content=0;URL=?view=pegawai&tampil=Data>";
    }
}elseif($_GET['tampil']=='Tambah'){ 
	$z = mysql_query("SELECT * from pegawai where id_pegawai='K001'");
	$c = mysql_num_rows($z);
		if($c>0){
			$query = mysql_query("SELECT MAX(id_pegawai) as max_id from pegawai");
			$data = mysql_fetch_array($query);
			$id_max = $data['max_id'];
			$sort_num = substr($id_max, 1, 3);
			$sort_num++;
			$new_code = 'K'.sprintf("%03s", $sort_num);
		}else{
		   $new_code = 'K001';
		} ?>

	<div class="animated fadeIn">
        <div class="card">
            <div class="card-header">
                <strong>Input Pegawai</strong>
            </div>
            <form method="post" enctype="multipart/form-data">
	            <div class="card-body card-block">
	                <div class="form-group">
	                    <label class=" form-control-label">ID Pegawai</label>
	                    <div class="input-group">
	                        <div class="input-group-addon"><i class="fa fa-lock"></i></div>
	                        <input class="form-control" value="<?php echo $new_code; ?>" type="text" name="id_pegawai" readonly="">
	                    </div>
	                </div>
	                <div class="form-group">
	                    <label class=" form-control-label">Nama Pegawai</label>
	                    <div class="input-group">
	                        <div class="input-group-addon"><i class="ti-agenda"></i></div>
	                        <input class="form-control" type="text" name="nm_pegawai" placeholder="Nama Pegawai ..">
	                    </div>
	                </div>
	                <div class="form-group">
	                        <label class=" form-control-label">Jenis Kelamin</label>&nbsp;&nbsp;&nbsp;&nbsp;
	                          <div class="input-group">
	                            <div class="input-group-addon"><i class="fa fa-bolt"></i></div>
	                            <select class="form-control" name="jenkel">
	                            	<option value="" hidden="">--Pilih Jenis Kelamin--</option>
	                                <option value="Laki-Laki">Laki-Laki</option>
	                                <option value="Perempuan">Perempuan</option>
	                            </select>
	                        </div>
	                    </div>
	                <div class="form-group">
	                    <label class=" form-control-label">Tanggal Lahir</label>
	                    <div class="input-group">
	                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
	                        <input class="form-control" type="date" name="tgl_lahir">
	                    </div>
	                </div>
	                <div class="form-group">
	                    <label class=" form-control-label">No Handphone</label>
	                    <div class="input-group">
	                        <div class="input-group-addon"><i class="fa fa-phone"></i></div>
	                        <input class="form-control" type="number" name="nohp" placeholder="+62 8XX XXXX XXXX">
	                    </div>
	                </div>
	                <div class="form-group">
	                    <label class=" form-control-label">Username</label>
	                    <div class="input-group">
	                        <div class="input-group-addon"><i class="fa fa-lock"></i></div>
	                        <input class="form-control" type="text" name="username" placeholder="Username ..">
	                    </div>
	                </div>
	                <div class="form-group">
	                    <label class=" form-control-label">Password</label>
	                    <div class="input-group">
	                        <div class="input-group-addon"><i class="fa fa-lock"></i></div>
	                        <input class="form-control" type="text" name="password" placeholder="Password ..">
	                    </div>
	                </div>
	                <div class="form-group">
	                    <label class=" form-control-label">Bagian</label>
	                    <div class="input-group">
	                        <div class="input-group-addon"><i class="fa fa-archive"></i></div>
	                        <select name="bagian" class="form-control">
	                            <option value="" hidden="">-- Pilih Bagian --</option>
	                            <option value="Kurir">Kurir</option>
	                            <option value="Gudang">Gudang</option>
	                            <option value="Produksi">Produksi</option>
	                        </select>
	                    </div>
	                </div>
	                <div class="form-group">
	                    <label class=" form-control-label">Alamat</label>
	                    <div class="input-group">
	                        <div class="input-group-addon"><i class="ti-clip"></i></div>
	                        <textarea class="form-control" name="alamat" placeholder="Alamat .."></textarea>
	                    </div>
	                </div>
	                <button type="submit" name="simpan" class="btn btn-primary"><i class="ti-save"></i>&nbsp;&nbsp;&nbsp;Simpan</button>
	                <a href="?view=pegawai" class="btn btn-danger"><i class="ti-arrow-left"></i>&nbsp;&nbsp;Batal</a>
	            </div>
	        </form>
	    </div>
	</div>
<?php
    if(isset($_POST['simpan'])){
        $id_pegawai = $_POST['id_pegawai'];
        $nm_pegawai = $_POST['nm_pegawai'];
        $nohp = $_POST['nohp'];
        $jenkel = $_POST['jenkel'];
        $tgl_lahir = $_POST['tgl_lahir'];
        $alamat = $_POST['alamat'];
        $username = $_POST['username'];
        $password = $_POST['password'];
        $level = $_POST['level'];
        $bagian = $_POST['bagian'];

        mysql_query("INSERT INTO pegawai(
        	id_pegawai,
        		nm_pegawai,
        			tgl_lahir,
        				jenkel,
        	 				nohp,
    							bagian,
    								alamat,
    									username,
    										password
		) VALUES(
			'$id_pegawai',
				'$nm_pegawai',
					'$tgl_lahir',
						'$jenkel',
							'$nohp',
								'$bagian',
									'$alamat',
										'$username',
	    										'$password'
		)");
        echo"<script>alert ('Data Berhasil Disimpan')</script>";
        echo"<meta http-equiv='refresh' content=0;URL=?view=pegawai&tampil=Data>";
 }}elseif($_GET['tampil']=='Hapus'){
 	if(mysql_query("DELETE FROM pegawai WHERE id_pegawai = '$_GET[Kode]'")){
 		echo"<script>alert ('Data Berhasil Dihapus')</script>";
        echo"<meta http-equiv='refresh' content=0;URL=?view=pegawai&tampil=Data>";
 	}
 } ?>