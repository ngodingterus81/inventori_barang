<br>
<?php if (!isset($_GET['tampilan'])): ?>

    <div class="col-xl-6 col-lg-6">
        <div class="card bg-success">
            <a href="?view=kirim&kurir=proses&tampilan=Konfirmasi&Kode=<?=$_GET['Kode']?>" class="btn btn-outline-secondary">
                <div class="card-body">
                    <div class="stat-widget-one">
                        <div class="stat-icon dib"><i class="fa fa-truck text-white border-white"></i></div>
                        <div class="stat-content dib">
                            <div class="stat-digit text-white"><p></p><h4><b>Konfirmasi Pengriman Barang</b></h4></div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-xl-6 col-lg-6">
        <div class="card bg-warning">
            <a href="?view=kirim&kurir=proses&tampilan=Pengalihan&Kode=<?=$_GET['Kode']?>" class="btn btn-outline-secondary">
                <div class="card-body">
                    <div class="stat-widget-one ">
                        <div class="stat-icon dib"><i class="fa fa-wheelchair text-white border-white"></i></div>
                        <div class="stat-content dib">
                            <div class="stat-digit text-white"><p></p><h4><b>Pengalihan Kurir</b></h4></div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
<?php  elseif ($_GET['tampilan']=="Konfirmasi"): ?>
    <form method="post" enctype="multipart/form-data">
        <div class="card-body card-block">
            <div class="form-group">
                <label class=" form-control-label"><b>Tanggal Terima</b></label>
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                    <input class="form-control" type="date" name="TglTerima">
                </div>
            </div>
            <div class="form-group">
                <label class=" form-control-label"><b>Upload Bukti Penerimaan</b></label>
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-image"></i></div>
                    <input class="form-control" type="file" name="BuktiTerima">
                </div>
            </div>
            <button type="submit" name="simpan" class="btn btn-primary"><i class="ti-save"></i>&nbsp;&nbsp;&nbsp;Simpan</button>
            <a href="?view=kirim&kurir=proses&Kode=<?=$_GET['Kode']?>" class="btn btn-danger"><i class="ti-arrow-left"></i> Batal</a>
        </div>
    </form>
    <?php if(isset($_POST['simpan'])){
        $NamaGambar = $_FILES['BuktiTerima']['name'];
        if(mysql_query("UPDATE g_kirim SET TglTerima = '$_POST[TglTerima]', GambarPenerima = '$NamaGambar', Status = 'Terima' WHERE SJGudang = '$_GET[Kode]'")){
            if(is_file($_FILES['BuktiTerima']['name'])){
                unlink($_FILES['BuktiTerima']['name']);
                if(move_uploaded_file($_FILES['BuktiTerima']['tmp_name'], 'images/kirim/'.$_FILES['BuktiTerima']['name'])){
                    echo "<script>alert('Data Berhasil Diunggah..!!');location.href='?view=g_permintaan_barang&tampil=Data'</script>";
                }
            }else{
               if(move_uploaded_file($_FILES['BuktiTerima']['tmp_name'], 'images/kirim/'.$_FILES['BuktiTerima']['name'])){
                    echo "<script>alert('Data Berhasil Diunggah..!!');location.href='?view=g_permintaan_barang&tampil=Data'</script>";
                } 
            }
        }

    } ?>
<?php elseif ($_GET['tampilan']=="Pengalihan"): ?>
    <form method="post" enctype="multipart/form-data">
        <div class="card-body card-block">
            <div class="form-group">
                <label class=" form-control-label"><b>Pilih Kurir Yang Menggantikan Pengiriman</b></label>
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-user"></i></div>
                    <select name="Pengalihan" id="Pengalihan" class="form-control">
                        <option value="" hidden="">--Pilih Pergantian Kurir</option>
                        <?php $e = mysql_query("SELECT * FROM pegawai WHERE bagian =
                    'Kurir'");
                    while ($f=mysql_fetch_array($e)) { ?>
                        <option value="<?=$f['id_pegawai']?>"><?=$f['nm_pegawai']?></option>
                    <?php } ?> 
                    </select>
                </div>
            </div>
            <button type="submit" name="simpan" class="btn btn-primary"><i class="ti-save"></i>&nbsp;&nbsp;&nbsp;Simpan</button>
            <a href="?view=kirim&kurir=proses&Kode=<?=$_GET['Kode']?>" class="btn btn-danger"><i class="ti-arrow-left"></i> Batal</a>
        </div>
    </form>
    <?php if(isset($_POST['simpan'])){
        $Kurir = $_FILES['BuktiTerima']['name'];
        if(mysql_query("UPDATE g_kirim SET KodeKurir = '$_POST[Pengalihan]' WHERE SJGudang = '$_GET[Kode]'")){
            echo "<script>alert('Data Berhasil Diunggah..!!');location.href='?view=g_permintaan_barang&tampil=Data'</script>";
        }

    } ?>
<?php endif ?>